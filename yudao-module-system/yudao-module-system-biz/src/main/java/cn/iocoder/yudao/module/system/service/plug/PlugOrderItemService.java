package cn.iocoder.yudao.module.system.service.plug;

import cn.iocoder.yudao.module.system.dal.dataobject.plug.PlugOrderItemDO;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 订单项 Service 接口
 *
 * @author zhusy
 */
public interface PlugOrderItemService extends IService<PlugOrderItemDO> {

}
