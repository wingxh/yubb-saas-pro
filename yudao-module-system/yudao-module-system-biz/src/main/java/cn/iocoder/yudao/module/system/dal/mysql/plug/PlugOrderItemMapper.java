package cn.iocoder.yudao.module.system.dal.mysql.plug;

import cn.iocoder.yudao.framework.mybatis.core.mapper.BaseMapperX;
import cn.iocoder.yudao.module.system.dal.dataobject.plug.PlugOrderItemDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单项 Mapper
 *
 * @author zhusy
 */
@Mapper
public interface PlugOrderItemMapper extends BaseMapperX<PlugOrderItemDO> {

}
