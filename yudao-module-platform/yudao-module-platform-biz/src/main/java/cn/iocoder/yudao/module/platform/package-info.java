/**
 * platform 模块下，我们放平台业务，支撑上层的核心业务。
 * 例如说：用户、部门、权限、数据字典等等
 *
 * 1. Controller URL：以 /platform/ 开头，避免和其它 Module 冲突
 * 2. DataObject 表名：以 platform_ 开头，方便在数据库中区分
 */
package cn.iocoder.yudao.module.platform;
