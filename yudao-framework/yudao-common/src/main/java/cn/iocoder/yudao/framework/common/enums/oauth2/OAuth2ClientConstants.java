package cn.iocoder.yudao.framework.common.enums.oauth2;

/**
 * OAuth2.0 客户端的通用枚举
 *
 * @author 圣钰科技
 */
public interface OAuth2ClientConstants {

    /**
     * 平台端使用
     */
    String CLIENT_ID_DEFAULT = "default";

    /**
     * 租户端使用
     */
    String CLIENT_ID_TENANT = "tenant";

}
