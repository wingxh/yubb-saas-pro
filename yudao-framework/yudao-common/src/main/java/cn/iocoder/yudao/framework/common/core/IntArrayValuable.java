package cn.iocoder.yudao.framework.common.core;

/**
 * 可生成 Int 数组的接口
 *
 * @author 圣钰科技
 */
public interface IntArrayValuable {

    /**
     * @return int 数组
     */
    int[] array();

}
